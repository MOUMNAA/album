public class Main {

  public static void main(String[] args) {

    CollectionMusicale collectionMusicale =
        new CollectionMusicale("C:\\Users\\Hp\\Downloads\\ateliers\\album\\src\\albums2.txt");
//    List<Album> albums = collectionMusicale.albums;
//    System.out.println(albums.toString());
    System.out.println("getNombreAlbumsDistincts : "+collectionMusicale.getNombreAlbumsDistincts());
    System.out.println("recherche par artiste U: "+collectionMusicale.rechercherParArtiste("U").length);
    System.out.println("recherche par artiste u: "+collectionMusicale.rechercherParArtiste("u").length);
    for(Album alb:collectionMusicale.rechercherParArtiste("u")){
      System.out.println(alb);
    }

    System.out.println("recherche par artiste b: "+collectionMusicale.rechercherParArtiste("b").length);
    System.out.println("recherche par artiste x: "+collectionMusicale.rechercherParArtiste("x").length);
    System.out.println("recherche par artiste x: "+collectionMusicale.rechercherParArtiste("x").length);
    System.out.println("recherche par artiste '': "+collectionMusicale.rechercherParArtiste("").length);

    System.out.println("recherche par titre 'JOSH': "+collectionMusicale.rechercherParTitre("JOSH").length);

    for(Album alb:collectionMusicale.rechercherParTitre("JOSH")){
      System.out.println(alb);
    }
    System.out.println("recherche par titre 'joshua': "+collectionMusicale.rechercherParTitre("joshua").length);
    System.out.println("recherche par titre 'martin': "+collectionMusicale.rechercherParTitre("martin").length);
    System.out.println("recherche par titre 'null': "+collectionMusicale.rechercherParTitre(null).length);

    System.out.println("rechercher Par Periode :1982 1988 "+collectionMusicale.rechercherParPeriode(1982,1988).length);
    for(Album alb:collectionMusicale.rechercherParPeriode(1982,1988)){
      System.out.println(alb);
    }
    System.out.println("rechercher Par Periode :1969 1977 "+collectionMusicale.rechercherParPeriode(1969,1977).length);
    System.out.println("rechercher Par Periode :1969 1969 "+collectionMusicale.rechercherParPeriode(1969,1969).length);
    System.out.println("rechercher Par Periode :1988 1988 "+collectionMusicale.rechercherParPeriode(1988,1988).length);
    System.out.println("rechercher Par Periode :1988 1970 "+collectionMusicale.rechercherParPeriode(1988,1970).length);


    System.out.println("rechercher Par Annee 1988 :"+collectionMusicale.rechercherParAnnee(1988).length);
    for(Album alb:collectionMusicale.rechercherParAnnee(1988)){
      System.out.println(alb);
    }
    System.out.println("rechercher Par Annee 1987 :"+collectionMusicale.rechercherParAnnee(1987).length);
    System.out.println("rechercher Par Annee 1985 :"+collectionMusicale.rechercherParAnnee(1984).length);



    System.out.println("rechercher Par Evaluation 3 "+collectionMusicale.rechercherParEvaluation(3).length);
    for(Album alb:collectionMusicale.rechercherParEvaluation(3)){
      System.out.println(alb);
    }
    System.out.println("rechercher Par Evaluation 1 "+collectionMusicale.rechercherParEvaluation(1).length);

    System.out.println("Moyenne Evaluations u2 "+collectionMusicale.getMoyenneEvaluations("u2"));
    System.out.println("Moyenne Evaluations u3 "+collectionMusicale.getMoyenneEvaluations("u3"));

    System.out.println("Moyenne Evaluations u1 "+collectionMusicale.getMoyenneEvaluations("u1"));
    System.out.println("Moyenne Evaluations null "+collectionMusicale.getMoyenneEvaluations(null));


    String[] genres = new String[] {"",null};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);
    genres = null;
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);
    genres = new String[] {"Rock",null};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);

    genres = new String[] {"rock",null};
    System.out.println("rechercher Par Genres rock"+collectionMusicale.rechercherParGenres(genres).length);

    genres = new String[] {"Ro",null};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);
    genres = new String[] {"fo",null};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);

    genres = new String[] {"folkarrrrr"};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);

    genres = new String[] {"folk"};
    System.out.println("rechercher Par Genres "+collectionMusicale.rechercherParGenres(genres).length);

    genres = new String[] {"post"};
    System.out.println("rechercher Par sous Genres post "+collectionMusicale.rechercherParGenres(genres).length);
  }
}

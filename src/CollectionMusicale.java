import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Cette classe  modélise une collection d’albums de musique
 * @author
 * @version
 */
public class CollectionMusicale {

    //-----------------------------------
    // ATTRIBUTS ET CONSTANTES DE CLASSE
    //-----------------------------------

    //-----------------------------------
    // ATTRIBUTS D'INSTANCE
    //-----------------------------------
    private List<Album> albums;    // La liste des albums appartenant à cette collection.


    //-------------------
    // CONSTRUCTEURS
    //-------------------


    /**
     * Lit chacun des albums contenus dans le fichier texte ficAlbums, et les insère dans la liste albums
     * (dans l’ordre où ils se présentent)
     * @param ficAlbums Le chemin du fichier qui contient la liste des albums de cette collection.
     */
    public CollectionMusicale(String ficAlbums) {
        List<List<String>> lignes = mapFichierToListValues(ficAlbums);
        this.albums = lignes.stream().map(ligne -> mapLigneToAlbum(ligne)).collect(Collectors.toList());
    }

    /**
     * compare les titres des 2 album fournies en parametre sans tenir compte de la casse
     * @param e1 1er album
     * @param e2 2eme album
     * @return -1 si le titre de 1er album est avant le 2eme,1 sinon
     */
    private static int compare(Album e1, Album e2) {
        int resultat = 0;
        if (e1.getTitre().toUpperCase().compareTo(e2.getTitre().toUpperCase()) < 0) {
            resultat = -1;
        } else if (e1.getTitre().toUpperCase().compareTo(e2.getTitre().toUpperCase()) > 0) {
            resultat = 1;
        }
        return resultat;
    }

    /**
     * Lit chacun des albums contenus dans le fichier texte path, et les insère dans un stream
     * (dans l’ordre où ils se présentent)
     * @param path Le chemin du fichier qui contient la liste des albums .
     * @return stream des lignes de fichier
     */
    private Stream<String> getLignesFichier(String path) {
        Stream<String> lines;
        try {
            lines = Files.lines(Paths.get(path));
        } catch (Exception e) {
            lines = new ArrayList<String>().stream();
        }
        return lines;
    }

    /**
     * Lit chacun des albums contenus dans le fichier texte path, et les insère dans une liste
     * @param path Le chemin du fichier qui contient la liste des albums .
     * @return Liste des valeurs contenus dans le fichier
     */
    private List<List<String>> mapFichierToListValues(String path) {

        List<List<String>> values = new ArrayList<>();
        getLignesFichier(path).filter(s -> (!(s.startsWith("#")) && (!s.trim().equals(""))))
            .collect(Collectors.toList())
            .forEach(ligne -> values.add(mapLigneToListValues(ligne)));
        return values;
    }

    /**
     * transforme une ligne de fichier text en liste des valeurs
     * @param ligne ligne de fichier text
     * @return liste des valeurs
     */
    private List<String> mapLigneToListValues(String ligne) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(ligne)) {
            rowScanner.useDelimiter("\\|");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }

    /**
     * map une liste de type String en instance Album
     * @param ligne ligne de fichier text
     * @return instance de Album
     */
    private Album mapLigneToAlbum(List<String> ligne) {

        Album album = new Album(Integer.valueOf(ligne.get(0).trim()),
            ligne.get(2).trim(), ligne.get(3).trim(),
            Integer.valueOf(ligne.get(1).trim()),
            Integer.valueOf(ligne.get(4).trim()));
        List<String> genres = Arrays.asList(ligne.get(5).split(","));
        genres.forEach(s -> album.ajouterGenre(s.trim()));

        if (ligne.size() == 7) {
            List<String> sousGenres = Arrays.asList(ligne.get(6).split(","));
            sousGenres.forEach(s -> album.ajouterSousGenre(s.trim()));
        }

        return album;
    }

    /**
     * Retourne le nombre total d’albums distincts (sans doublons) dans cette collection
     * @return  retourne le nombre total d’albums distincts (sans doublons) dans cette collection
     */
    public int getNombreAlbumsDistincts() {
        return (int) albums.stream().distinct().count();
    }

    /**
     * Retourne un tableau contenant tous les albums de cette collection
     * ayant été faits par l’artiste donné en paramètre
     * @param artiste L’artiste des albums recherchés.
     * @return un tableau contenant tous les albums de cette collection
     * ayant été faits par l’artiste donné en paramètre
     */
    public Album[] rechercherParArtiste(String artiste) {
        Album[] albumArray;
        if (artiste == null) {
            albumArray = new Album[0];
        } else {
            List<Album> albumList = this.albums.stream()
                .filter(s -> s.getArtiste().toUpperCase().contains(artiste.toUpperCase()))
                .sorted(CollectionMusicale::compare)
                .distinct().collect(Collectors.toList());
            albumArray = new Album[albumList.size()];
            albumList.toArray(albumArray);
        }
        return albumArray;
    }

    /**
     * Retourne un tableau contenant tous les albums de cette collection
     * dont le titre contient le titre donné en paramètre
     * @param titre Le titre des albums recherchés.
     * @return un tableau contenant tous les albums de cette collection
     * dont le titre contient le titre donné en paramètre
     */
    public Album[] rechercherParTitre(String titre) {
        Album[] albumArray;
        if (titre == null) {
            albumArray = new Album[0];
        } else {
            List<Album> albumList = this.albums.stream()
                .filter(s -> s.getTitre().toUpperCase().contains(titre.toUpperCase()))
                .sorted(CollectionMusicale::compare)
                .distinct().collect(Collectors.toList());
            albumArray = new Album[albumList.size()];
            albumList.toArray(albumArray);
        }
        return albumArray;
    }

    /**
     * retourne un tableau contenant tous les albums de cette collection dont l’année
     * de sortie se trouve entre anneeMin et anneeMax inclusivement.
     * @param anneeMin L’année de sortie minimum des albums recherchés.
     * @param anneeMax L’année de sortie maximum des albums recherchés.
     * @return un tableau contenant tous les albums de cette collection dont l’année
     * de sortie se trouve entre anneeMin et anneeMax inclusivement.
     */
    public Album[] rechercherParPeriode(int anneeMin, int anneeMax) {
        Album[] albumArray;
        if (anneeMin > anneeMax) {
            albumArray = new Album[0];
        } else {
            List<Album> albumList = this.albums.stream()
                .filter(s -> (s.getAnnee() >= anneeMin) && (s.getAnnee() <= anneeMax))
                .sorted((e1, e2) -> {
                    int resultat = 0;
                    if (e1.getAnnee() < e2.getAnnee()) {
                        resultat = -1;
                    } else if (e1.getAnnee() > e2.getAnnee()) {
                        resultat = 1;
                    }
                    return resultat;
                })
                .distinct().collect(Collectors.toList());
            albumArray = new Album[albumList.size()];
            albumList.toArray(albumArray);
        }
        return albumArray;
    }

    /**
     * retourne un tableau contenant tous les albums de cette collection dont l’année de sortie est égale à annee.
     * @param annee L’année de sortie des albums recherchés.
     * @return un tableau contenant tous les albums de cette collection dont l’année de sortie est égale à annee.
     */
    public Album[] rechercherParAnnee(int annee) {
        Album[] albumArray;
        List<Album> albumList = this.albums.stream().filter(s -> (s.getAnnee() == annee))
            .sorted(CollectionMusicale::compare)
            .distinct().collect(Collectors.toList());
        albumArray = new Album[albumList.size()];
        albumList.toArray(albumArray);
        return albumArray;
    }

    /**
     * retourne un tableau contenant tous les albums de cette collection dont l’évaluation est égale à eval.
     * @param eval L’évaluation des albums recherchés.
     * @return un tableau contenant tous les albums de cette collection dont l’évaluation est égale à eval.
     */
    public Album[] rechercherParEvaluation(int eval) {
        Album[] albumArray;
        List<Album> albumList = this.albums.stream().filter(s -> (s.getEvaluation() == eval))
            .sorted(CollectionMusicale::compare)
            .distinct().collect(Collectors.toList());
        albumArray = new Album[albumList.size()];
        albumList.toArray(albumArray);

        return albumArray;
    }

    /**
     * verifie si le tableau fourni en parametre contient que des elements nuls
     * @param array le tableau a verifier
     * @return true si tous les element de le tableau sont nuls ,false sinon
     */
    private Boolean isNullArray(String[] array) {
        Boolean isNull = true;
        for (String elemnt : array) {
            if (elemnt != null && !elemnt.equals("")) {
                isNull = false;
            }
        }
        return isNull;
    }

    /**
     * transforme un tableau de string en List de String
     * @param array un tableau de string a transformer
     * @return list de String
     */
    private List<String> arrayToList(String[] array) {
        List<String> values = new ArrayList<>();
        for (String elemnt : array) {
            if (elemnt != null && !elemnt.equals("")) {
                values.add(elemnt);
            }
        }
        return values;
    }

    /**
     * verifie si list contient une valeur value
     * @param list Une liste sur laquelle on désire faire la recherche.
     * @param value la valeur a rechercher
     * @return true si list contient value,false sinon
     */
    private boolean listContain(List<String> list, String value) {
        return list.stream()
            .filter(s -> value.toUpperCase().contains(s.toUpperCase()))
            .count() > 0;
    }

    /**
     * verifie si un album donné contient au moin un genre de la liste
     * @param album un album donné dont on désire faire la recherche
     * @param genres Un tableau contenant les genres avec lesquels on désire faire la recherche.
     * @return true si un album donné contient au moin un genre de la liste, false sinon
     */
    private Boolean albumContainGenre(Album album, List<String> genres) {
        Boolean exist;
        Iterable<String> iterable = () -> album.iterateurGenres();

        exist = StreamSupport
            .stream(iterable.spliterator(), false)
            .anyMatch(element -> listContain(genres, element));
        if (!exist) {
            iterable = () -> album.iterateurSousGenres();
            exist = StreamSupport
                .stream(iterable.spliterator(), false)
                .anyMatch(element -> listContain(genres, element));
        }
        return exist;
    }

    /**
     * retourne un tableau contenant tous les albums de cette collection qui ont
     * comme genres OU sous-genres TOUS les genres contenus dans le tableau passé en paramètre
     * @param genres Un tableau contenant les genres avec lesquels on désire faire la recherche.
     * @return un tableau contenant tous les albums de cette collection qui ont
     * comme genres OU sous-genres TOUS les genres contenus dans le tableau passé en paramètre
     */
    public Album[] rechercherParGenres(String[] genres) {
        Album[] tableauAlbum;
        if (genres == null || genres.length == 0) {
            tableauAlbum = new Album[0];
        } else if (isNullArray(genres)) {
            List<Album> albumList = this.albums.stream()
                .sorted(CollectionMusicale::compare)
                .distinct().collect(Collectors.toList());
            tableauAlbum = new Album[albumList.size()];
            albumList.toArray(tableauAlbum);
        } else {
            List<String> listGenres = arrayToList(genres);
            List<Album> albumList = this.albums.stream()
                .filter(s -> albumContainGenre(s, listGenres))
                .sorted((e1, e2) -> {
                    int resultat = compare(e1, e2);
                    return resultat;
                })
                .distinct().collect(Collectors.toList());
            tableauAlbum = new Album[albumList.size()];
            albumList.toArray(tableauAlbum);
        }
        return tableauAlbum;
    }

    /**
     * retourne la moyenne des évaluations de tous les albums ayant été faits par l’artiste donné en paramètre
     * @param artiste L’artiste des albums sur lesquels on veut calculer la moyenne des évaluations.
     * @return la moyenne des évaluations de tous les albums ayant été faits par l’artiste donné en paramètre
     */
    public double getMoyenneEvaluations(String artiste) {
        double moyenneEvaluations = 0;
        if (artiste == null || artiste.trim().equals("")) {
            moyenneEvaluations = 0;
        } else {
            List<Album> albumList = this.albums.stream()
                .filter(s -> s.getArtiste().toUpperCase().equals(artiste.toUpperCase()))
                .collect(Collectors.toList());
            if (!albumList.isEmpty()) {
                int somme = 0;
                for (Album album : albumList) {
                    somme += album.getEvaluation();
                }
                moyenneEvaluations = (float) somme / albumList.size();
            }
        }
        return moyenneEvaluations;
    }
}
